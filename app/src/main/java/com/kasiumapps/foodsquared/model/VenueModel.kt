package com.kasiumapps.foodsquared.model

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
data class VenueModel(val id: String, val name: String, val location: FourSquareLocation)