package com.kasiumapps.foodsquared.model

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
data class VenueResponseModel(val response: ResponseModel)

data class ResponseModel(val venues: List<VenueModel>)