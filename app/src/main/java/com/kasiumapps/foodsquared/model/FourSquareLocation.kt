package com.kasiumapps.foodsquared.model

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
data class FourSquareLocation(val lat: Double, val lng: Double, val formattedAddress: Array<String>) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FourSquareLocation

        if (lat != other.lat) return false
        if (lng != other.lng) return false
        if (!formattedAddress.contentEquals(other.formattedAddress)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = lat.hashCode()
        result = 31 * result + lng.hashCode()
        result = 31 * result + formattedAddress.contentHashCode()
        return result
    }
}