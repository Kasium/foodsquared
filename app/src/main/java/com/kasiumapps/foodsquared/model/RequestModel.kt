package com.kasiumapps.foodsquared.model

import com.google.android.gms.maps.model.LatLng

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
data class RequestModel(val swLatLng: LatLng, val neLatLng: LatLng)