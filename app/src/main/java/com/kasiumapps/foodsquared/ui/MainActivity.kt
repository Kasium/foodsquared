package com.kasiumapps.foodsquared.ui

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.kasiumapps.foodsquared.R
import com.kasiumapps.foodsquared.databinding.ActivityMainBinding
import com.kasiumapps.foodsquared.model.RequestModel
import com.kasiumapps.foodsquared.model.VenueModel
import com.kasiumapps.foodsquared.ui.venue.VenueListViewModel

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
class MainActivity: AppCompatActivity() {

    private val LOCATION_PERMISSION_REQUEST = 1
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: VenueListViewModel
    private lateinit var mapFragment: SupportMapFragment
    private val markers = hashMapOf<String, Marker>()
    private var dialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mapFragment = SupportMapFragment()
        supportFragmentManager.beginTransaction().add(binding.fragmentContainer.id, mapFragment).commit()

        viewModel = ViewModelProviders.of(this).get(VenueListViewModel::class.java)
        binding.viewModel = viewModel

        mapFragment.getMapAsync { map ->
            run {
                setupMap(map)
            }
        }

        viewModel.venueData.observe(this, Observer { newVenueDataReceived(it) })
    }

    override fun onStart() {
        super.onStart()
        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //Nice to have: Give explanation for request of permission and check `shouldShowRequestPermissionRationale()`
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION),
                LOCATION_PERMISSION_REQUEST)
        } else {
            enableMyLocationOnMap()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == LOCATION_PERMISSION_REQUEST && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            enableMyLocationOnMap()
        }
    }

    override fun onDestroy() {
        dismissDialogIfNeeded()
        super.onDestroy()
    }

    private fun setupMap(map: GoogleMap) {
        setRealCameraIdleListener(map)
        map.uiSettings.isRotateGesturesEnabled = false
        map.setOnMarkerClickListener {
            //Temporarily disable the camera idle listener to scroll to marker
            map.setOnCameraIdleListener {
                setRealCameraIdleListener(map)
            }
            false
        }
        map.setOnInfoWindowClickListener { marker ->
            val id = markers.keys.find { markers.get(it) == marker }
            id?.let {
                viewModel.findVenueModel(it)?.let { venueModel ->  showVenueDetail(venueModel) }
            }
        }
    }

    private fun enableMyLocationOnMap() {
        mapFragment.getMapAsync { map ->
            run {
                map.isMyLocationEnabled = true
            }
        }
    }

    private fun setRealCameraIdleListener(map: GoogleMap) {
        map.setOnCameraIdleListener {
            mapCameraIdle(map)
        }
    }

    private fun mapCameraIdle(map: GoogleMap) {
        // Nice to have --> Allow for turning of the map and still retrieve South West and North East locations
        viewModel.loadVenues(RequestModel(map.projection.visibleRegion.nearLeft, map.projection.visibleRegion.farRight))
    }

    private fun newVenueDataReceived(venueList: List<VenueModel>) {
        markers.forEach { it.value.remove() }
        mapFragment.getMapAsync { map ->
            run {
                venueList.forEach {
                    markers.put(it.id, map.addMarker(MarkerOptions()
                        .position(LatLng(it.location.lat, it.location.lng))
                        .title(it.name)))
                }
            }
        }
    }

    private fun showVenueDetail(venueModel: VenueModel) {
        dismissDialogIfNeeded()
        val builder: AlertDialog.Builder? = this.let {
            AlertDialog.Builder(it)
        }
        var message = getString(R.string.venue_detail_message, venueModel.name, venueModel.id)
        if (venueModel.location.formattedAddress.isNotEmpty()) {
            var locationMessage = getString(R.string.venue_detail_message_location)
            venueModel.location.formattedAddress.forEach { locationMessage += "\n$it" }
            message += locationMessage
        }

        builder?.setMessage(message)?.setTitle(venueModel.name)
            ?.setPositiveButton(R.string.ok) { _, _ -> dismissDialogIfNeeded() }
            ?.setOnDismissListener { dialog = null }

        dialog = builder?.create()
        dialog?.show()
    }

    private fun dismissDialogIfNeeded() {
        dialog?.dismiss()
        dialog = null
    }
}