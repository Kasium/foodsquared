package com.kasiumapps.foodsquared.ui.venue

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.kasiumapps.foodsquared.BuildConfig
import com.kasiumapps.foodsquared.api.VenueAPI
import com.kasiumapps.foodsquared.base.BaseViewModel
import com.kasiumapps.foodsquared.model.RequestModel
import com.kasiumapps.foodsquared.model.VenueModel
import com.kasiumapps.foodsquared.model.VenueResponseModel
import com.kasiumapps.foodsquared.utils.extension.toCustomString
import com.kasiumapps.foodsquared.utils.schedular.SchedulerProvider
import javax.inject.Inject

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
class VenueListViewModel : BaseViewModel() {

    @Inject
    lateinit var venueAPI: VenueAPI

    @Inject
    lateinit var schedulerProvider: SchedulerProvider

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()

    val venueData: MutableLiveData<List<VenueModel>> = MutableLiveData(listOf())
    private var venueCache = listOf<VenueModel>()
    private val TAG = "VenueListViewModel"

    fun loadVenues(requestModel: RequestModel) {
        //Directly update venueData according to new requestModel/Map view port from cache
        updateRequestedVenueData(requestModel)
        //Retrieve new venue data from remote
        compositeDisposable.add(venueAPI.getVenues(
            requestModel.swLatLng.toCustomString(),
            requestModel.neLatLng.toCustomString(), BuildConfig.FOURSQUARE_CLIENT_ID,
            BuildConfig.FOURSQUARE_CLIENT_SECRET
        )
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .doOnSubscribe { onRetrievePostListStart() }
            .doOnTerminate { onRetrievePostListFinish() }
            .subscribe(
                { onRetrievePostListSuccess(it, requestModel) },
                { onRetrievePostListError(it) }
            ))
    }

    fun findVenueModel(id: String): VenueModel? {
        return venueCache.find { it.id == id }
    }

    private fun onRetrievePostListStart() {
        loadingVisibility.value = View.VISIBLE
    }

    private fun onRetrievePostListFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrievePostListSuccess(
        venueResponseModel: VenueResponseModel,
        requestModel: RequestModel
    ) {
        venueCache = venueCache.union(venueResponseModel.response.venues).toList()
        updateRequestedVenueData(requestModel)
    }

    private fun onRetrievePostListError(throwable: Throwable) {
        Log.d(TAG, "Unable to retrieve venues.", throwable)
    }

    private fun insideRequestSpace(lat: Double, lng: Double, sw: LatLng, ne: LatLng): Boolean {
        return lat <= ne.latitude && lng <= ne.longitude && lat >= sw.latitude && lng >= sw.longitude
    }

    private fun updateRequestedVenueData(requestModel: RequestModel) {
        //Only set to mutable live data venues that are within the requestModel/view port of map
        venueData.value = venueCache.filter {
            insideRequestSpace(
                it.location.lat,
                it.location.lng,
                requestModel.swLatLng,
                requestModel.neLatLng
            )
        }
    }
}