package com.kasiumapps.foodsquared.base

import androidx.lifecycle.ViewModel
import com.kasiumapps.foodsquared.di.NetworkModule
import com.kasiumapps.foodsquared.di.SchedulersModule
import com.kasiumapps.foodsquared.di.component.DaggerViewModelInjector
import com.kasiumapps.foodsquared.di.component.ViewModelInjector
import com.kasiumapps.foodsquared.ui.venue.VenueListViewModel
import io.reactivex.disposables.CompositeDisposable

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
abstract class BaseViewModel : ViewModel() {

    var injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule())
        .schedulersModule(SchedulersModule())
        .build()
    val compositeDisposable = CompositeDisposable()

    init {
        inject()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    /**
     * Injects the required dependencies
     */
    fun inject() {
        when (this) {
            is VenueListViewModel -> injector.inject(this)
        }
    }
}