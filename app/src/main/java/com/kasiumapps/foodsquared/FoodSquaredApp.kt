package com.kasiumapps.foodsquared

import android.app.Application

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
class FoodSquaredApp : Application() {
}