package com.kasiumapps.foodsquared.api

import com.kasiumapps.foodsquared.model.VenueResponseModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
interface VenueAPI {

    @GET("search")
    fun getVenues(@Query("sw") sw: String,
                  @Query("ne") ne: String,
                  @Query("client_id") clientId: String,
                  @Query("client_secret") clientSecret: String,
                  @Query("categoryId") categoryId: String = "4d4b7105d754a06374d81259",
                  @Query("intent") intent: String = "browse",
                  @Query("v") v: String = "20191118"):
            Observable<VenueResponseModel>
}