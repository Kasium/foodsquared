package com.kasiumapps.foodsquared.utils.schedular

import io.reactivex.Scheduler

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
interface SchedulerProvider {
    fun io(): Scheduler
    fun ui(): Scheduler
}