package com.kasiumapps.foodsquared.utils.extension

import com.google.android.gms.maps.model.LatLng

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
fun LatLng.toCustomString(): String {
    return StringBuilder(50).append(latitude).append(",").append(longitude).toString()
}