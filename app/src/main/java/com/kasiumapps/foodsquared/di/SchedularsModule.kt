package com.kasiumapps.foodsquared.di

import com.kasiumapps.foodsquared.utils.schedular.SchedulerProvider
import com.kasiumapps.foodsquared.utils.schedular.SchedulerProviderImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
@Module
open class SchedulersModule {
    @Provides
    @Singleton
    open fun schedulers(inst: SchedulerProviderImpl): SchedulerProvider = inst
}