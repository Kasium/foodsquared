package com.kasiumapps.foodsquared.di.component

import com.kasiumapps.foodsquared.di.NetworkModule
import com.kasiumapps.foodsquared.di.SchedulersModule
import com.kasiumapps.foodsquared.ui.venue.VenueListViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
@Singleton
@Component(modules = [(NetworkModule::class), (SchedulersModule::class)])
interface ViewModelInjector {

    fun inject(venueListViewModel: VenueListViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
        fun schedulersModule(schedulersModule: SchedulersModule): Builder
    }
}