package com.kasiumapps.foodsquared.ui.venue

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.android.gms.maps.model.LatLng
import com.kasiumapps.foodsquared.api.VenueAPI
import com.kasiumapps.foodsquared.di.TestNetworkModule
import com.kasiumapps.foodsquared.di.TestSchedulersModule
import com.kasiumapps.foodsquared.di.component.DaggerTestViewModelInjector
import com.kasiumapps.foodsquared.model.*
import io.reactivex.Observable
import org.junit.*
import org.junit.rules.TestRule
import org.mockito.Mockito
import javax.inject.Inject


/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
class VenueListViewModelTest {
    //Needed to observe Mutable Live data outside of main looper thread
    @Rule
    @JvmField
    val rule: TestRule = InstantTaskExecutorRule()

    @Inject
    lateinit var venueAPI: VenueAPI

    private val component = DaggerTestViewModelInjector.builder()
        .networkModule(TestNetworkModule())
        .schedulersModule(TestSchedulersModule())
        .build()

    private val mockLocationZero = LatLng(0.0, 0.0)
    private val mockLocationOne = LatLng(1.0, 1.0)
    private val mockVenueModel = VenueModel(
        "fakeID",
        "",
        FourSquareLocation(mockLocationZero.latitude, mockLocationZero.longitude, arrayOf())
    )
    private val mockSingleVenueResponseModel =
        VenueResponseModel(ResponseModel(listOf(mockVenueModel)))
    private val mockVenueModel2 = VenueModel(
        "fakeID2",
        "",
        FourSquareLocation(mockLocationZero.latitude, mockLocationZero.longitude, arrayOf())
    )
    private val mockSingleVenueResponseModel2 =
        VenueResponseModel(ResponseModel(listOf(mockVenueModel2)))
    private val mockVenueModel3 = VenueModel(
        "fakeID3",
        "",
        FourSquareLocation(mockLocationOne.latitude, mockLocationOne.longitude, arrayOf())
    )
    private val mockSingleVenueResponseModel3 =
        VenueResponseModel(ResponseModel(listOf(mockVenueModel3)))
    private lateinit var venueListViewModel: VenueListViewModel
    private var dataCount = 0
    private var expectedDataCount = 0

    @Before
    fun setup() {
        component.inject(this)
        dataCount = 0
        venueListViewModel = VenueListViewModel()
        venueListViewModel.injector = component
        venueListViewModel.inject()
    }

    @Test
    fun loadAVenueInLiveDataTest() {
        expectedDataCount = 1
        val venueAPIObservable = Observable.just(mockSingleVenueResponseModel)
        Mockito.doReturn(venueAPIObservable)
            .`when`(venueAPI).getVenues(
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString()
            )

        venueListViewModel.venueData.observeForever {
            if (it.isNotEmpty()) {
                dataCount = it.size
                Assert.assertEquals(mockVenueModel, it.first())
            }
        }
        venueListViewModel.loadVenues(RequestModel(mockLocationZero, mockLocationZero))

        venueAPIObservable
            .test()
            .assertResult(mockSingleVenueResponseModel)
    }

    @Test
    fun loadCachedVenueInLiveDataInViewPort() {
        expectedDataCount = 2
        val venueAPIObservable = Observable.just(mockSingleVenueResponseModel)
        Mockito.doReturn(venueAPIObservable)
            .`when`(venueAPI).getVenues(
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString()
            )

        venueListViewModel.venueData.observeForever {
            if (it.isNotEmpty()) {
                dataCount = it.size
            }
        }
        venueListViewModel.loadVenues(RequestModel(mockLocationZero, mockLocationZero))
        Assert.assertEquals(1, venueListViewModel.venueData.value?.size)
        Assert.assertEquals(mockVenueModel, venueListViewModel.venueData.value?.first())

        val venueAPIObservable2 = Observable.just(mockSingleVenueResponseModel2)
        Mockito.doReturn(venueAPIObservable2)
            .`when`(venueAPI).getVenues(
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString()
            )

        venueListViewModel.loadVenues(RequestModel(mockLocationZero, mockLocationZero))
        Assert.assertEquals(2, venueListViewModel.venueData.value?.size)
        Assert.assertTrue(venueListViewModel.venueData.value?.contains(mockVenueModel) == true)
        Assert.assertTrue(venueListViewModel.venueData.value?.contains(mockVenueModel2) == true)
    }


    @Test
    fun loadCachedVenueInLiveDataExceptWhenOutsideViewport() {
        expectedDataCount = 1
        //Load cache
        val venueAPIObservable = Observable.just(mockSingleVenueResponseModel3)
        Mockito.doReturn(venueAPIObservable)
            .`when`(venueAPI).getVenues(
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString()
            )

        venueListViewModel.venueData.observeForever {
            if (it.isNotEmpty()) {
                dataCount = it.size
            }
        }
        venueListViewModel.loadVenues(RequestModel(mockLocationOne, mockLocationOne))
        Assert.assertEquals(1, venueListViewModel.venueData.value?.size)
        Assert.assertEquals(mockVenueModel3, venueListViewModel.venueData.value?.first())

        val venueAPIObservable2 = Observable.just(mockSingleVenueResponseModel2)
        Mockito.doReturn(venueAPIObservable2)
            .`when`(venueAPI).getVenues(
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString()
            )

        venueListViewModel.loadVenues(RequestModel(mockLocationZero, mockLocationZero))
        Assert.assertEquals(1, venueListViewModel.venueData.value?.size)
        Assert.assertFalse(venueListViewModel.venueData.value?.contains(mockVenueModel3) == true)
    }
    
    @After
    fun checkCount() {
        Assert.assertEquals(expectedDataCount, dataCount)
    }
}