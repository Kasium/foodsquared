package com.kasiumapps.foodsquared.di.component

import com.kasiumapps.foodsquared.di.TestNetworkModule
import com.kasiumapps.foodsquared.di.TestSchedulersModule
import com.kasiumapps.foodsquared.ui.venue.VenueListViewModelTest
import dagger.Component
import javax.inject.Singleton

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */
@Singleton
@Component(modules = [(TestNetworkModule::class), (TestSchedulersModule::class)])
interface TestViewModelInjector: ViewModelInjector {

    fun inject(venueListViewModelTest: VenueListViewModelTest)

    @Component.Builder
    interface Builder {
        fun build(): TestViewModelInjector

        fun networkModule(networkModule: TestNetworkModule): Builder
        fun schedulersModule(schedulersModule: TestSchedulersModule): Builder
    }
}