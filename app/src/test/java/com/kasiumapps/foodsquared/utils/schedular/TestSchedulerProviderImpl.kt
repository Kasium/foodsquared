package com.kasiumapps.foodsquared.utils.schedular

import io.reactivex.Scheduler
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Copyright © 2019 KasiumApps
 * Developed by KasiumApps.
 * All rights reserved.
 */

@Singleton
class TestSchedulerProviderImpl @Inject constructor() : SchedulerProvider {

    override fun ui(): Scheduler {
        return io.reactivex.schedulers.Schedulers.trampoline()
    }

    override fun io(): Scheduler {
        return io.reactivex.schedulers.Schedulers.trampoline()
    }
}