# FoodSquared

Simple restaurant search using the FourSquare API:
* Displays restaurants on a map, if user allows, around his/her location
  * Uses the [FourSquare Search API](https://developer.foursquare.com/docs/api/venues/search) to query for restaurants
* Loads more restaurants when the user pans the map.
  * Caches results in-memory (no persistent cache).
  * Reads restaurants from the cache to show results early, but only if the restaurants fit within the user’s current viewport.
* Includes a simple restaurant detail pop-up.

## Build configuration

To be able to build the project you will need to create a `repository.local.properties` file in the base directory and add the following properties:
```
4square.client.id=
4square.client.secret=
```
They need to be filled with a valid [FourSquare Userless Auth](https://developer.foursquare.com/docs/api/configuration/authentication) client ID and client Secret.
You will also need to add a valid [Google Maps API Key](https://developers.google.com/maps/documentation/android-sdk/get-api-key) to the strings file `/app/src/main/res/values/secrets.xml`.  
If you don't have any lying around for testing contact the developer at kasium.apps@gmail.com

## Future work

If time allows what would be the next things to improve:
* Add CI/CD configuration for automated testing and deployment
* Improve test coverage
* Improve user experience, providing feedback when there is no internet connection, explaining the reason for the location permission, panning directly to a users location when it is found, etc...
* Add real detail page with more information  